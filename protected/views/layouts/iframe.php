<?php /* @var $this Controller */ ?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>TestApp</title>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" type="text/css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->

<!-- disable iPhone inital scale -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<!-- media queries css -->
<?php /*?><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" /><?php */?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/media-queries.css" rel="stylesheet" type="text/css">

<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
<![endif]-->

<!-- css3-mediaqueries.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
</head>
<body style="background:none repeat scroll 0 0 #353546 !important;">
    <div id="main">
        <div class="container">
        	<?php echo $content; ?> 
    	</div>
    </body>
</html>
