<?php /* @var $this Controller */ ?>
<?php /*?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?><?php */?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>TestApp</title>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" type="text/css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->

<!-- disable iPhone inital scale -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<!-- media queries css -->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/media-queries.css" rel="stylesheet" type="text/css">

<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
<![endif]-->

<!-- css3-mediaqueries.js for IE less than 9 -->
<!--[if lt IE 9]>
	<script src="js/css3-mediaqueries.js"></script>
<![endif]-->


</head>

<body>
	
    <div id="main">
    	
       <div id="headerContainer">
       
       		<div class="header">
            	<div class="logo">
                	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" alt="logo">
                </div>
                <div class="nav">
                	<ul>
                    	<li><a href="#">Home</a></li>
                        <li><a href="#" class="active">Infos</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Help</a></li>
                    </ul>
                </div>
                <div class="user">
                	
                	<?php 
					if( Yii::app()->user->isGuest )
					{
					?>
                    <input type="button" class="button" name="login" value="Login" id='addByIframe' href="/site/login">
                    <?php
						$this->widget('application.extensions.fancybox.EFancyBox', 
									array(
										'target'=>'#addByIframe',
										'config'=>array('type'=>'iframe','width'=>'50%','height'=>'98%','scrolling'=>'yes',
										'afterClose'=>'js:function() { window.location.reload(); }',
									)
								)
						);
					?>
                        <br><br>
                        <input type="button" class="button button_1" name="create" value="Create Account">
                    <?php	
					}
					else
					{   
						echo CHtml::link(Yii::t('main','Logout'),array('Logout'),array('class'=>'button'));
					?>
						<br><br>
                        <input type="button" class="button button_1" name="create" value="logged in by <?php echo Yii::app()->user->name ?>">
					<?php
					}

					?>
                	
                </div>
            </div>
       
       </div> 
       
       <div class="container">
        
		<?php echo $content; ?> 
       
           <div class="clear"></div>
            
           <div class="footer">
            
                <div class="content">
                    <div class="social-widget">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/twitter-post.png" alt="twitter">
                        Nie musisz już więcej samodzielnie drukować dokumentów, które mają zostać zawarte w przesyłce, adresować kopert, udawać się do urzędów pocztowych i stać w długich kolejkach - możesz to zrobić wprost z komputera przez Internet.<br>
                        <span class="right">- Web developer</span>
                    </div>
                    
                    <div class="clear"></div>
                    
                    <div class="footCol">
                        <h3>Aktualności</h3>
                        Wyliczanie zobowiązań podatkowych (PIT, VAT) i ubezpieczeniowych  odbywa się automatycznie - wystarczy jedno kliknięcie.<br><br>
                        <input type="text" class="textbox" placeholder="wprowadź swój adres email" id="email" name="email" value=""><span class="gap1"><input type="submit" class="button_2 button_3" name="subscribe" value="Subskrybuj"></span>
                        <div class="clear"></div>
                        
                        <div class="social-icon">
                            <span><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/twitter-icon.png" alt="twitter"></a></span><span><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/facebook-icon.png" alt="facebook"></a></span><span><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/linkedin-icon.png" alt="linkedin"></a></span><br>
                            
                            <span class="right">Copyright © 2014 TEST.COM</span>
                        </div>
                    </div>
                    <div class="footCol right">
                        <h3>Formularz kontaktowy</h3>
                        <div class="col-2">
                            <input type="text" class="textbox textbox_1" placeholder="" id="name" name="name" value="">
                            <input type="text" class="textbox textbox_1" placeholder="" id="email" name="email" value="">
                            <input type="text" class="textbox textbox_1" placeholder="" id="phone" name="phone" value="">
                        </div>
                        <div class="col-2 right">
                            <textarea placeholder="" class="textbox textbox_1 txtarea" id="query" name="query"></textarea><br>
                            
                            <span><input type="submit" class="button_2 button_3" name="send" value="Wyślij"></span>
                        </div>
                    </div>
                </div>
            
           </div>
               
    </div>
    
    </body>
</html>
